"""Test flake8_type_ignore error codes."""

from __future__ import annotations

import ast
from typing import Any, List, Tuple, Type

import pytest

from flake8_type_ignore import TypeIgnoreChecker, flake8_error_codes


def _run_type_checker(
    code: str,
) -> List[Tuple[int, int, str, Type[Any]]]:
    """Parse the supplied code and run the checker.

    Returns:
        The results the checker returns to flake8, cast to a list to
        reduce testing boilerplate.
    """
    tree = ast.parse(code)
    checker = TypeIgnoreChecker(tree=tree, lines=[code])
    return list(checker.run())


def test_bare_ignore() -> None:
    """Test error on non-specific ignores."""
    results = _run_type_checker("x: str = 1  # type: ignore")
    assert len(results) == 1
    _, _, message, _ = results[0]
    assert message.startswith("TI100")


@pytest.mark.parametrize("code,error", (flake8_error_codes.items()))
def test_ignore_specific_error_codes(code: str, error: str) -> None:
    """Test error on each supported ignore code."""
    results = _run_type_checker(f"x: str = 1  # type: ignore[{code}]")
    assert len(results) == 1
    _, _, message, _ = results[0]
    assert message.startswith(error)


def test_malformatted_type_comment() -> None:
    """Test that malformatted type ignores fail."""
    results = _run_type_checker("x: str = 1  # type: ignore this one")
    assert len(results) == 1
    _, _, message, _ = results[0]
    assert message.startswith("TI001")


def test_invalid_ignore_code() -> None:
    """Test that nonexistent ignore codes fail."""
    results = _run_type_checker("x: str = 1  # type: ignore[a-fake-code]")
    assert len(results) == 1
    _, _, message, _ = results[0]
    assert message.startswith("TI002")
